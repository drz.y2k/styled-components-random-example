import React,{Fragment} from 'react'
import ReactDOM from 'react-dom'
import Content from './components/Content'

class Main extends React.Component {
  render() {
    return (
      <Fragment>
        <h1>Hello world!</h1>
        <Content />
      </Fragment>
    );
  }
}

ReactDOM.render(<Main />, document.getElementById('root'))
